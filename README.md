# Crossref Funder Registry

The Funder Registry provides a common taxonomy of international funding body names that funding data participants should use to normalize funder names and IDs for deposit with Crossref.

The list should be used to present authors with a pre-populated "Funder Name" option at the time of submission, and can also be used to match funder names extracted from papers.

The list is available to download as an RDF file, and is freely available under a CC0 license waiver.

## Latest Version

The latest version of the registry is `v1.33`.

**PLEASE NOTE that we discovered some errors in v1.33 with encoded characters and continue to have problems loading a new version of the registry. We are looking into the timeframe for a new version. In the meantime please revert to using [v1.32](https://gitlab.com/crossref/open_funder_registry/-/releases/v1.32)**

Funder count: 25,411.

25,164 active funders, 247 defunct or replaced.


A .csv file of the most recent version of the Registry can [also be downloaded](https://doi.crossref.org/funderNames?mode=list) Note that there is no version history at this time for the .csv format.
